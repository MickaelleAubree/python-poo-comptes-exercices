class Compte():
    def __init__(self, no_compte, nom_pers, solde):
        self.no_compte = no_compte
        self.nom_pers = nom_pers
        self.solde = solde

    def retrait(self, withdraw):
        """Method to withdraw money from account"""
        self.solde = self.solde - withdraw
        # print("Vous avez retirer {} de votre compte".format(str(amount)))
        print("Vous avez retiré {} de votre compte.".format(str(withdraw)))
        print("Vous avez maintenant ", self.solde)

    def versement(self, transfert):
        """Method to add money to account"""
        self.solde = self.solde + int(transfert)
        print("Vous avez ajouté {} à votre compte".format(str(transfert)))
        print("Votre solde est mainteant de {}".format(self.solde))

    def afficherSolde(self):
        """Method to show account amount"""
        print("Vous avez ", self.solde, " sur votre compte")

class CompteCourant(Compte):
    def __init__(self, no_compte, nom_pers, solde, decouvert, agios):
        self.decouvert = decouvert
        self.agios = agios
        super().__init__(no_compte, nom_pers, solde)
        print("New account created")
            # ATTRIBUTS DECOUVERTS & POURCENTAGE AGIOS

    def appliquerAgios(self, agios):
        """Method to calculate agios"""
        agios = (self.solde * 0.25)/100
        if self.solde < 0:
            self.solde - agios
        print("Ajoutez des sous !!")

class CompteEpargne(Compte):
    def __init__(self, no_compte, nom_pers, solde, interets):
        self.interets = interets
        super().__init__(self, no_compte, nom_pers, solde)
        print("Compte epargne crée")

    def appliquerInterets(self, interets):
        """Method to calculate interest on savings"""
        interets = (self.solde * 0.85)/100
        self.solde = self.solde + interets
        print("Vous gagnez des sous ! ")


compte = Compte("1234", "Micka", 120000000)
print(compte.nom_pers)
compte.afficherSolde()
compte.retrait(5000)
compte_courant = CompteCourant("6789", "Kassy", 35000, 500, 0.8)
print(compte_courant.nom_pers)
compte_ep = CompteEpargne("5678", "Linda", 45000, 3)
print(compte_ep.nom_pers)
## Projet pas finit!! Micka aime chercher les solutions

